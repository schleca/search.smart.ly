# SearchSmart.ly #

# Boilerplate #

### Tools

Using react starter kit https://github.com/Stanko/react-redux-webpack2-boilerplate with some custom updates

The project is using the following packages:
React,
React router,
Redux,
Redux Thunk,
Redux Logger,
React Router Redux,
Webpack 2 (development and production config),
Hot Module Replacement,
Babel,
SASS with autoprefixing,
ESLint,
`es6-promise` and `isomorphic-fetch`,
File imports relative to the app root,

and Redux Dev Tool Extension
(Add chrome app Redux DevTools https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=ennp)

### Setup

```
$ npm install
```

### Running in dev mode

In source/js/api/index.js :
Change api variable from api_production to api_development

```
$ npm run dev
```

Wait a bit and `http://localhost:3000/` will open in your browser


### Build (production)

Build will be placed in the `build` folder.

```
$ npm run build
```


### Linting

This project is using [eslint-config-airbnb](https://www.npmjs.com/package/eslint-config-airbnb).

```
$ npm run lint
```

### Heroku

This project is currently deployed to Heroku at searchsmartly.heroku.com

### Note:

The code for the server is on an additional repository, for hosting with Heroku (bitbucket.org ... search-simply-server).



