import { push } from 'react-router-redux';
import { reset } from 'actions/properties';

export const UPDATE_FIELD = 'UPDATE_FIELD';
export const FORCE_VALIDATION = 'FORCE_VALIDATION';

function updateField(key, value, error) {
  return {
    type: UPDATE_FIELD,
    key,
    value,
    error,
  };
}

function forceValidation(validationErrors) {
  return {
    type: FORCE_VALIDATION,
    validationErrors,
  };
}

export function updateFieldAction(key, value, error) {
  return function (dispatch) {
    dispatch(updateField(key, value, error));
  };
}

export function submitFormAction() {
  return function (dispatch, getState) {
    const { form, formErrors } = getState().landing;
    const inputsKeys =
      ['minPrice', 'maxPrice', 'address', 'commute', 'bedrooms', 'bathrooms', 'gyms', 'supermarkets', 'schools', 'parks'];

    const noValidationErrors = inputsKeys.every((inputsKey: string) => formErrors[inputsKey] === false);

    dispatch(forceValidation(!noValidationErrors));

    if (noValidationErrors) {
      localStorage.removeItem('search');
      localStorage.setItem('reload', true);
      localStorage.setItem('search', JSON.stringify(form));
      dispatch(push('/properties'));
    }
  };
}
