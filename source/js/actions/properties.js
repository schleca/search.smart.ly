import api from 'api';
import { push } from 'react-router-redux';

export const GET_PROPERTIES_START = 'GET_PROPERTIES_START';
export const GET_PROPERTIES_SUCCESS = 'GET_PROPERTIES_SUCCESS';
export const GET_PROPERTIES_ERROR = 'GET_PROPERTIES_ERROR';
export const RESET_PROPERTIES = 'RESET_PROPERTIES';

function getPropertiesStart() {
  return {
    type: GET_PROPERTIES_START,
  };
}

function getPropertiesSuccess(response) {
  return {
    type: GET_PROPERTIES_SUCCESS,
    response,
  };
}

function getPropertiesError(error) {
  return {
    type: GET_PROPERTIES_ERROR,
    error,
  };
}

function resetProperties() {
  return {
    type: RESET_PROPERTIES,
  };
}

export function getPropertiesAction() {
  return function (dispatch) {
    const reload = localStorage.getItem('reload');

    if (reload) {
      const form = JSON.parse(localStorage.getItem('search'));
      if (!form || form === undefined) {
        dispatch(push('/'));
      } else {
        dispatch(getPropertiesStart());
        api.getPropertiesAPI(form)
          .then(response => {
            localStorage.removeItem('reload');
            return dispatch(getPropertiesSuccess(response));
          })
          .catch(error => dispatch(getPropertiesError(error)));
      }
    }
  };
}

export function reset() {
  return function (dispatch) {
    dispatch(resetProperties());
  };
}
