import api from 'api';
import { push } from 'react-router-redux';

export const GET_PROPERTY_START = 'GET_PROPERTY_START';
export const GET_PROPERTY_SUCCESS = 'GET_PROPERTY_SUCCESS';
export const GET_PROPERTY_ERROR = 'GET_PROPERTY_ERROR';

function getPropertyStart() {
  return {
    type: GET_PROPERTY_START,
  };
}

function getPropertySuccess(response) {
  return {
    type: GET_PROPERTY_SUCCESS,
    response,
  };
}

function getPropertyError(error) {
  return {
    type: GET_PROPERTY_ERROR,
    error,
  };
}

export function getPropertyAction(propertyId) {
  return function (dispatch) {
    const form = JSON.parse(localStorage.getItem('search'));
    if (!form || form === undefined) {
      dispatch(push('/'));
    } else {
      dispatch(getPropertyStart());

      api.getPropertyAPI(propertyId, form)
        .then(response => dispatch(getPropertySuccess(response)))
        .catch(error => dispatch(getPropertyError(error)));
    }
  };
}
