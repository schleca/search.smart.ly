import 'es6-promise';
import 'isomorphic-fetch';

const createFeatures = form => {
  return [
    { type: 'gyms', rank: parseInt(form.gyms, 10) },
    { type: 'supermarkets', rank: parseInt(form.supermarkets, 10) },
    { type: 'schools', rank: parseInt(form.schools, 10) },
    { type: 'parks', rank: parseInt(form.parks, 10) },
  ];
};

const createRequest = form => {
  const request = Object.assign({}, form, {
    lat: form.address.location.lat,
    lng: form.address.location.lng,
    features: createFeatures(form),
  });

  delete request.address;
  delete request.gyms;
  delete request.supermarkets;
  delete request.schools;
  delete request.parks;

  return request;
};

const api_development = 'http://localhost:4000';
const api_production = 'https://searchsimplyserver.herokuapp.com';

function getPropertiesAPI(form) {
  return fetch(`${ api_production }/api/properties`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(createRequest(form)),
  })
  .then(response => {
    if (response.status >= 400) {
      throw new Error('Bad response from server');
    }
    return response.json();
  });
}

function getPropertyAPI(propertyId, form) {
  return fetch(`${ api_production }/api/property/${ propertyId }`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({
      features: createFeatures(form),
      location: form.address.location
    }),
  })
  .then(response => {
    if (response.status >= 400) {
      throw new Error('Bad response from server');
    }
    return response.json();
  });
}

export default {
  getPropertiesAPI,
  getPropertyAPI,
};
