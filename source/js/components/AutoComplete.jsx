import React from 'react';
import Geosuggest from 'react-geosuggest';

class AutoComplete extends React.Component {
  static propTypes = {
    onChange: React.PropTypes.func,
    value: React.PropTypes.object,
    updatedValue: React.PropTypes.string,
    styleName: React.PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.onChange(this.props.updatedValue, { location: event.location, label: event.label });
  }

  render() {
    return (
      <label className={ `${ this.props.styleName }` } >
        <img src={ require('../../assets/img/geo.png') } alt='Icon' />
        <Geosuggest
          ref={ el => this._geoSuggest = el }
          placeholder='Your commute location'
          initialValue={ this.props.value.label }
          location={ new google.maps.LatLng(51.509865, -0.118092) }
          radius='20'
          onSuggestSelect={ this.handleChange }
        />
      </label>
    );
  }
}

export default AutoComplete;
