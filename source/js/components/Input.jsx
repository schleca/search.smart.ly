import React from 'react';

class CustomInput extends React.Component {
  static propTypes = {
    onChange: React.PropTypes.func,
    inputLabel: React.PropTypes.string,
    value: React.PropTypes.number,
    updatedValue: React.PropTypes.string,
    styleName: React.PropTypes.string,
    placeholder: React.PropTypes.string,
    type: React.PropTypes.string,
  };

  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    event.preventDefault();
    this.props.onChange(this.props.updatedValue, event.target.value);
  }

  render() {
    return (
      <label htmlFor={ this.props.updatedValue }>
        <span className={ `input${ this.props.styleName }` } >{this.props.inputLabel}</span>
        <input
          className={ `${ this.props.styleName }` }
          type={ this.props.type }
          value={ this.props.value }
          onChange={ this.handleChange }
          placeholder={ this.props.placeholder }
          id={ this.props.updatedValue }
        />
      </label>
    );
  }
}

export default CustomInput;
