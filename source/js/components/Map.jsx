import React, { Component } from 'react';
import { GoogleMap, Marker, withGoogleMap } from 'react-google-maps';

const GettingStartedGoogleMap = withGoogleMap(props => (
  <GoogleMap
    ref={ props.onMapLoad }
    defaultZoom={ 13 }
    center={ { lat: props.markers[0].position.lat, lng: props.markers[0].position.lng } }
    onClick={ props.onMapClick }
  >
    {props.markers.map(marker => (
      <Marker
        { ...marker }
        onRightClick={ () => props.onMarkerRightClick(marker) }
      />
    ))}
  </GoogleMap>
));

export default class SimpleMap extends Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className='mapDiv'>
        <GettingStartedGoogleMap
          containerElement={ <div className='mapContainer' /> }
          mapElement={ <div className='mapElement' /> }
          // onMapClick={ this.handleMapClick }
          markers={ this.props.markers }
          // onMarkerRightClick={ this.handleMarkerRightClick }
        />
      </div>
      // null // this is not working
    );
  }
}
