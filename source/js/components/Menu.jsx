import React from 'react';
import { push } from 'react-router-redux';
import { getStore } from 'index';

const Menu = () =>
  <div className='Menu'>
    <div className='menuLogo' onClick={ () => getStore().dispatch(push('/')) } >
      SearchSmart.ly
      <div className='beta'>
      Beta
      </div>
    </div>

    <div className='rightButtons'>
      <button className='back navButton' onClick={ () => getStore().dispatch(push('/properties')) }>
        <img src={ require('../../assets/img/back.png') } alt='Back' />
        Back
      </button>
      <button className='filter navButton' onClick={ () => getStore().dispatch(push('/')) } >
        <img src={ require('../../assets/img/filter.png') } alt='Search' />
        Search
      </button>
    </div>
  </div>;

export default Menu;
