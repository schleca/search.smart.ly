import React from 'react';
import { push } from 'react-router-redux';
import { getStore } from 'index';

const Nav = () =>
  <div className='nav'>
    <button className='back navButton' onClick={ () => getStore().dispatch(push('/properties')) }>
      <img src={ require('../../assets/img/back.png') } alt='Back' />
      Back
    </button>
    <button className='filter navButton' onClick={ () => getStore().dispatch(push('/')) } >
      <img src={ require('../../assets/img/filter.png') } alt='Search' />
      Search
    </button>
  </div>;

export default Nav;
