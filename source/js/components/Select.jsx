import React from 'react';
import Select from 'react-select';

class CustomSelect extends React.Component {
  static propTypes = {
    onChange: React.PropTypes.func,
    selectLabel: React.PropTypes.string,
    value: React.PropTypes.number,
    updatedValue: React.PropTypes.string,
    styleName: React.PropTypes.string,
    options: React.PropTypes.array,
  };

  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.onChange(this.props.updatedValue, event.value);
  }

  render() {
    return (
      <span className={ `selectWrapper ${ this.props.styleName }` }>
        <label htmlFor={ this.props.updatedValue }>
          {this.props.selectLabel}
        </label>
        <Select
          id={ this.props.updatedValue }
          className={ `${ this.props.styleName }` }
          value={ this.props.value }
          options={ this.props.options }
          onChange={ this.handleChange }
          resetValue={ true }
        />
      </span>
    );
  }
}

export default CustomSelect;
