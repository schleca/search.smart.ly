import React from 'react';
import Slider from 'rc-slider';

class CustomSlider extends React.Component {
  static propTypes = {
    onChange: React.PropTypes.func,
    sliderLabel: React.PropTypes.string,
    value: React.PropTypes.number,
  };

  constructor(props) {
    super(props);

    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(event) {
    this.props.onChange(event);
  }

  render() {
    const marks = {
      1: 'Nice to have',
      2: '',
      3: 'Super important!',
    };

    return (
      <div>
        <span>{this.props.sliderLabel}</span>
        <Slider
          min={ 1 }
          max={ 3 }
          defaultValue={ this.props.value }
          marks={ marks }
          onChange={ this.handleChange }
        />
      </div>
    );
  }
}

export default CustomSlider;
