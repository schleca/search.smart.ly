import React from 'react';
import Toggle from 'react-toggle';
import CustomSlider from './Slider';

class CustomToggle extends React.Component {
  static propTypes = {
    onChange: React.PropTypes.func,
    updatedValue: React.PropTypes.string,
    toggleLabel: React.PropTypes.string,
    value: React.PropTypes.number,
  };

  constructor() {
    super();

    this.state = {
      toggleSwitch: false,
      showSlider: false,
    };

    this.onChangeToggle = this.onChangeToggle.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  onChangeToggle(event) {
    this.setState({ toggleSwitch: event.target.checked, showSlider: event.target.checked });
    this.props.onChange(this.props.updatedValue, event.target.checked ? 1 : 0);
  }

  handleChange(event) {
    this.props.onChange(this.props.updatedValue, event);
  }

  render() {
    return (
      <div>
        <Toggle
          icons={ false }
          onChange={ this.onChangeToggle }
        />
        <div className='toggleLabel'>{ this.props.toggleLabel }</div>
        { this.state.showSlider &&
          <CustomSlider
            value={ this.props.value }
            onChange={ this.handleChange }
          /> }
      </div>
    );
  }
}

export default CustomToggle;
