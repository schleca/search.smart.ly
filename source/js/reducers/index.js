import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import properties from 'reducers/properties';
import property from 'reducers/property';
import landing from 'reducers/landing';

export default combineReducers({
  properties,
  property,
  landing,
  router: routerReducer,
});
