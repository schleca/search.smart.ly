import {
  UPDATE_FIELD,
  FORCE_VALIDATION,
} from 'actions/landing';

export const initialState = {
  form: {
    minPrice: 0,
    maxPrice: 0,
    address: {
      lat: 0,
      label: '',
      lng: 0,
    },
    commute: 0,
    bedrooms: 0,
    bathrooms: 0,
    gyms: 0,
    supermarkets: 0,
    schools: 0,
    parks: 0,
  },
  formErrors: {
    minPrice: false,
    maxPrice: false,
    address: true, // address is the only we make sure has a value
    commute: false,
    bedrooms: false,
    bathrooms: false,
    gyms: false,
    supermarkets: false,
    schools: false,
    parks: false,
  },
  forceValidation: false,
};

const actionsMap = {
  [UPDATE_FIELD]: (state, action) => {
    const { key, value, error } = action;
    return Object.assign({}, state, {
      form: Object.assign({}, state.form, {
        [key]: value,
      }),
      formErrors: Object.assign({}, state.formErrors, {
        [key]: error,
      }),
    });
  },
  [FORCE_VALIDATION]: (state, action) => {
    return Object.assign({}, state, {
      forceValidation: action.validationErrors,
    });
  },
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
