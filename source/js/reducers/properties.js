import {
  GET_PROPERTIES_START,
  GET_PROPERTIES_ERROR,
  GET_PROPERTIES_SUCCESS,
  RESET_PROPERTIES,
} from 'actions/properties';

const initialState = {
  isLoading: true,
  errorMessage: null,
  properties: [],
};

const actionsMap = {
  [GET_PROPERTIES_START]: state => {
    return Object.assign({}, state, {
      isLoading: true,
    });
  },
  [GET_PROPERTIES_ERROR]: (state, action) => {
    return Object.assign({}, state, {
      isLoading: false,
      errorMessage: action.error.message,
    });
  },
  [GET_PROPERTIES_SUCCESS]: (state, action) => {
    return Object.assign({}, state, {
      isLoading: false,
      properties: action.response,
      errorMessage: null,
    });
  },
  [RESET_PROPERTIES]: () => initialState,
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
