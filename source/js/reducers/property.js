import {
  GET_PROPERTY_START,
  GET_PROPERTY_ERROR,
  GET_PROPERTY_SUCCESS,
} from 'actions/property';

const initialState = {
  isLoading: true,
  errorMessage: null,
  property: {},
};

const actionsMap = {
  [GET_PROPERTY_START]: state => {
    return Object.assign({}, state, {
      isLoading: true,
    });
  },
  [GET_PROPERTY_ERROR]: (state, action) => {
    return Object.assign({}, state, {
      isLoading: false,
      errorMessage: action.error,
    });
  },
  [GET_PROPERTY_SUCCESS]: (state, action) => {
    return Object.assign({}, state, {
      isLoading: false,
      property: action.response[0],
      errorMessage: null,
    });
  },
};

export default function reducer(state = initialState, action = {}) {
  const fn = actionsMap[action.type];
  return fn ? fn(state, action) : state;
}
