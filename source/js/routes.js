import React, { Component } from 'react';
import { Route } from 'react-router';
import Landing from 'views/Landing';
import Properties from 'views/Properties';
import Property from 'views/Property';

export default class Routes extends Component {
  render() {
    return (
      <div>
        <Route exact path='/' component={ Landing } />
        <Route exact path='/properties' component={ Properties } />
        <Route exact path='/property/:propertyId' component={ Property } />
      </div>
    );
  }
}
