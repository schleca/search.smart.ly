import { connect } from 'react-redux';
import { updateFieldAction, submitFormAction } from 'actions/landing';

import React, { Component } from 'react';
import Menu from 'components/Menu';
import CustomToggle from 'components/Toggle';
import CustomInput from 'components/Input';
import CustomSelect from 'components/Select';
import AutoComplete from 'components/AutoComplete';

import { MIN_PRICE_VALUES, MAX_PRICE_VALUES, BEDROOMS_VALUES, BATHROOMS_VALUES } from './constants';

class Landing extends Component {

  static propTypes = {
    form: React.PropTypes.shape({
      minPrice: React.PropTypes.number,
      maxPrice: React.PropTypes.number,
      address: React.PropTypes.object,
      commute: React.PropTypes.number,
      bedrooms: React.PropTypes.number,
      bathrooms: React.PropTypes.number,
      gyms: React.PropTypes.number,
      supermarkets: React.PropTypes.number,
      schools: React.PropTypes.number,
      parks: React.PropTypes.number,
    }),
    updateFieldAction: React.PropTypes.func,
    submitFormAction: React.PropTypes.func,
  }

  constructor(props) {
    super(props);

    this.landingHandleChange = this.landingHandleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  landingHandleChange(field, newValue) {
    let value = newValue;
    let error = false;
    switch (field) {
      case 'address': error = value.label === ''; break;
      case 'bedrooms':
      case 'bathrooms':
        error = false;
        value = newValue === undefined ? 0 : newValue;
        break;
      case 'commute':
        error = false;
        value = parseFloat(newValue, 10);
        break;
      default: error = false;
    }

    this.props.updateFieldAction(field, value, error);
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.submitFormAction();
  }

  render() {
    return (
      <div className='landing'>
        <Menu />
        <div className='page'>
          <form className='formContent' onSubmit={ this.handleSubmit } >
            <h2>Search</h2>
            <AutoComplete
              styleName='autoSuggest'
              placeholder='Your commute location'
              type='text'
              value={ this.props.form.address }
              onChange={ this.landingHandleChange }
              updatedValue='address'
            />
            <div className='clearFloat'></div>
            <CustomInput
              styleName='spanBlock commuteInput inputLabel'
              inputLabel='Commute Radius (mi)'
              placeholder=''
              type='number'
              value={ this.props.form.commute }
              onChange={ this.landingHandleChange }
              updatedValue='commute'
            />
            <p className='inputLabel'>Price/Week (£)</p>
            <CustomSelect
              styleName='minPrice inputLabel'
              type='select'
              options={ MIN_PRICE_VALUES }
              value={ this.props.form.minPrice }
              onChange={ this.landingHandleChange }
              updatedValue='minPrice'
            />
            <CustomSelect
              styleName='maxPrice inputLabel'
              type='select'
              options={ MAX_PRICE_VALUES }
              value={ this.props.form.maxPrice }
              onChange={ this.landingHandleChange }
              updatedValue='maxPrice'
            />
            <p className='inputLabel'>Number of bedrooms and bathrooms</p>
            <CustomSelect
              styleName='inputLabel'
              type='select'
              options={ BEDROOMS_VALUES }
              value={ this.props.form.bedrooms }
              onChange={ this.landingHandleChange }
              updatedValue='bedrooms'
            />
            <CustomSelect
              styleName='inputLabel'
              type='select'
              options={ BATHROOMS_VALUES }
              value={ this.props.form.bathrooms }
              onChange={ this.landingHandleChange }
              updatedValue='bathrooms'
            />
            <CustomToggle
              toggleLabel='Gyms'
              value={ this.props.form.gyms }
              onChange={ this.landingHandleChange }
              updatedValue='gyms'
            />
            <CustomToggle
              toggleLabel='Supermarkets'
              value={ this.props.form.supermarkets }
              onChange={ this.landingHandleChange }
              updatedValue='supermarkets'
            />
            <CustomToggle
              toggleLabel='Schools'
              value={ this.props.form.schools }
              onChange={ this.landingHandleChange }
              updatedValue='schools'
            />
            <CustomToggle
              toggleLabel='Parks'
              value={ this.props.form.parks }
              onChange={ this.landingHandleChange }
              updatedValue='parks'
            />
            <div className='submit'>
              <input type='submit' value='Continue' />
            </div>
          </form>
        </div>
      </div>

    );
  }
}

const mapStateToProps = state => ({ ...state.landing });

const mapDispatchToProps = {
  updateFieldAction,
  submitFormAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Landing);
