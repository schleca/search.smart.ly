import { connect } from 'react-redux';
import React, { Component } from 'react';
import Menu from 'components/Menu';
import { getPropertiesAction } from 'actions/properties';
import { push } from 'react-router-redux';
import { getStore } from 'index';
import Spinner from 'components/Spinner';
import Nav from 'components/Nav';


class Properties extends Component {

  static propTypes = {
    isLoading: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    properties: React.PropTypes.array,
    getPropertiesAction: React.PropTypes.func,
  }

  componentWillMount() {
    this.props.getPropertiesAction();
  }

  render() {
    if (this.props.isLoading) {
      return (
        <div>
          <Menu />
          <div className='page'>
            <Spinner />
          </div>
        </div>
      );
    }

    return (
      <div className='properties'>
        <Menu />
        <Nav />
        <div className='page'>
          { this.props.errorMessage ? <div>There&apos;s an error!</div> :
          <div className='propertiesList'>
            { this.props.properties.length === 0 && 'No results' }
            { this.props.properties.map((property, index) => {
              const imgStyle = {
                backgroundImage: `url(${ property.image_url === '' ? require('../../../assets/img/home.png') : property.image_url })`,
                backgroundSize: 'cover',
              };

              let newScore = (property.smartscore); 

              if(newScore){
                newScore = newScore.toFixed(1);
              }

              // Change hsl color depending on score 0=red, 10=green
              const hue = ((newScore / 10) * 120).toString(10);
              const scoreColor = ['hsl(', hue, ',70%,50%)'].join('');

              const scoreStyle = {
                background: `${ scoreColor }`,
              };

              return (
                <div className='content-block' key={ index } onClick={ () => getStore().dispatch(push(`/property/${ property.listing_id }`)) } >
                  <div className='innerContainer'>
                    <span className='rank'>
                      {index + 1}
                    </span>
                    <span className='imgContainer'>
                      <div className='image' style={ imgStyle }></div>
                    </span>
                    <span className='descContainer' >
                      <div className='left'>
                        <span className='addressContainer'>
                          <p className='address'>{property.displayable_address}</p>
                        </span>
                        <div className='price'>£{property.price}pw</div>
                      </div>
                      <div className='amenities'>
                          {property.features.map((feature, index) => {
                            if (feature.found) {
                            const featureName = feature.type.substring(0, feature.type.length - 1);
                             return (
                              <img key={ index } src={ require(`../../../assets/img/${ featureName }.png`) } alt={ feature.type } />
                             );
                            }
                          })}
                      </div>
                      <div className='scoreContainer'>
                        { newScore ? <div style={ scoreStyle } className='scoreCircle'>
                          <div className='score' > {newScore} </div> 
                        </div> : null }
                      </div>
                    </span>
                  </div>
                </div>
              );
            })}
          </div>
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({ ...state.properties });

const mapDispatchToProps = {
  getPropertiesAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Properties);
