import { connect } from 'react-redux';
import React, { Component } from 'react';
import Menu from 'components/Menu';
import Nav from 'components/Nav';
import { getPropertyAction } from 'actions/property';
import SimpleMap from 'components/Map';
import Spinner from 'components/Spinner';

class Property extends Component {

  static propTypes = {
    isLoading: React.PropTypes.bool,
    errorMessage: React.PropTypes.string,
    property: React.PropTypes.object,
    match: React.PropTypes.object,
    getPropertyAction: React.PropTypes.func,
  }

  componentWillMount() {
    this.props.getPropertyAction(this.props.match.params.propertyId);
  }

  render() {
    if (this.props.isLoading) {
      return (
        <div>
          <Menu />
          <div className='page'>
            <Spinner />
          </div>
        </div>
      );
    }

    const list = this.props.property;
    const imgStyle = {
      backgroundImage: `url(${ list.image_url === '' ? require('../../../assets/img/home.png') : list.image_url })`,
      backgroundSize: 'cover',
      backgroundRepeat: 'no-repeat',
    };

    // Change hsl color depending on score 0=red, 10=green
    const hue = ((list.smartscore / 10) * 120).toString(10);
    const scoreColor = ['hsl(', hue, ',70%,50%)'].join('');

    const scoreStyle = {
      background: `${ scoreColor }`,
    };

    const markers = [{
        position: {
          lat: list.latitude,
          lng: list.longitude,
        },
        key: 'UK',
        defaultAnimation: 2,
      }];

    let newScore = list.smartscore;
    if(newScore){
      newScore = newScore.toFixed(1);
    }

    return (
      <div>
        <Menu />
        <Nav />
        <div className='page'>
          { this.props.errorMessage ? <div>There&apos;s an error!</div> :

          <div className='property'>
            <div>
            <div className='address'>
              <p>{list.displayable_address}</p>
            </div>
              <div className='imgContainer'>
                <div className='scoreContainer'>
                  { list.smartscore ? <div className='smartScore' style={scoreStyle}>{ newScore }</div> : null }
                </div>
                <div className='image' style={ imgStyle } src={ `${ list.image_url }` } alt='Property'>
                </div>
              </div>
              <div className='infoContainer'>
                <div className='left'>
                  <p className='price'>£{list.price}pw</p>
                  <p>{list.num_bedrooms} Bed{ list.num_bathrooms !== 0 ? <span> {list.num_bathrooms} Bath </span> : null }</p>
                  <p className='commute'>{list.distance} mile commute</p>
               </div>
                <div className='right'>
                  {list.features.map((feature, index) => {
                    const featureName = feature.type.substring(0, feature.type.length - 1);
                    if (feature.found) {
                     return (
                       <div className='assetPair' key={ index } >
                          <div className='iconContainer'>
                            <img src={ require(`../../../assets/img/${ featureName }.png`) } alt={ feature.type } />
                          </div>
                          <div className='nameContainer'>
                           <p><span className='distance'>{feature.distance.miles} miles</span>, {feature.name}</p>
                          </div>
                       </div>
                     );
                    }
                    return (
                     <div className='assetPair'>
                        <img src={ require(`../../../assets/img/${ featureName }.png`) } alt={ feature.type } />
                       <p> not found</p>
                     </div>
                    );
                  })}
                </div>
              </div>
              <div className='description'>
                <h3>The Property</h3>
                <p dangerouslySetInnerHTML={{__html: list.short_description}}></p>
              </div>
              <div className='links'>
                <div className='agent'>
                  <a className='mobile' href={`tel:${list.agent_phone}`}>Call Agent</a>
                  <div className='nonMobile'>Call Agent: {list.agent_phone}</div>
                </div>
                <div className='propertyLink'>
                  <a href={list.details_url} target="_blank" >View on Zoopla</a>
                </div>
              </div>
              <SimpleMap 
                markers={markers}
              />
            </div>
          </div>
          }
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({ ...state.property, ...state.router });

const mapDispatchToProps = {
  getPropertyAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(Property);
